# Wallpapers

This reporistory contains all my favorite wallpapers.
Most of them have been "extracted" from oficial linux distros or desktop environments.


## Wallpapers list


- `Altai-5120x2880.png`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `Canopee-3840x2160.png`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `Cascade-3840x2160.png`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `Cluster-3840x2160.png`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `FlyingKonqui-2560x1440.png`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `IceCold-5120x2880.png`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `Kokkini-3840x2160.png`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `MilkyWay-5120x2880.png`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `Opal-3840x2160.png`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `Patak-5120x2880.png`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `Autumn-2560x1440.jpg`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `BytheWater-2560x1440.jpg`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `ColdRipple-2560x1440.jpg`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `DarkestHour-2560x1440.jpg`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `EveningGlow-2560x1440.jpg`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `FallenLeaf-2560x1440.jpg`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `Flow-5120x2880.jpg`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `Grey-2560x1440.jpg`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `Kite-2560x1440.jpg`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `Next-5120x2880.jpg`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `OneStandsOut-2560x1440.jpg`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `PastelHills-2560x1440.jpg`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `Path-2560x1440.jpg`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `SafeLanding-5120x2880.jpg`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `Shell-5120x2880.jpg`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `summer_1am-2560x1440.jpg`: Wallpaper from the default KDE Plasma desktop 5.24 installation
- `Volna-5120x2880.jpg`: Wallpaper from the default KDE Plasma desktop 5.24 installation
